﻿using Domain.Shared.Enums;
using EFCore.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace EFCore.Extensions
{
    // đây là class để tạo ra dữ liệu mẫu trong database SQL Server
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            #region Category thoi su va cac bai viet
            modelBuilder.Entity<Categories>().HasData(
                    new Categories()
                    {
                        CategoryId = Guid.Parse("7C318165-4FD6-4FE2-A85C-069B08AE611F"),
                        CategoryName = "Thời sự",
                        DateCreated = DateTime.Now,
                        Status = Status.Active,
                    }
                    );

            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Chồng thả thính với 'sugar baby'",
                Status = Status.Active,
                Image = "https://i1-ngoisao.vnecdn.net/2020/12/02/shutterstock1481861243-1606886-2241-9135-1606886554.jpg?w=300&h=180&q=100&dpr=1&fit=crop&s=T07hahBCERXkZ6HbbbaboA",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("7C318165-4FD6-4FE2-A85C-069B08AE611F"),
                Summary = "Tôi chụp màn hình những tin nhắn gạ tình của chồng với 'sugar baby' đưa chồng xem nhưng anh vẫn chối."

            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Ảnh sinh nhật 19 tuổi của Công chúa Aiko'",
                Status = Status.Active,
                Image = "https://i-ngoisao.vnecdn.net/2020/12/03/congchua1-7039-1606986354.jpg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("7C318165-4FD6-4FE2-A85C-069B08AE611F"),
                Summary = "Công chúa Aiko đón sinh nhật tuổi 19 hôm 1/12 khi đang tham gia các khóa học qua mạng tại trường đại học do ảnh hưởng của đại dịch Covid-19."
            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Cô gái chụp ảnh cưới một mình sau 3 tuần bị hủy hôn",
                Status = Status.Active,
                Image = "https://i-ngoisao.vnecdn.net/2020/12/03/mm-1-4343-1606986393.jpg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("7C318165-4FD6-4FE2-A85C-069B08AE611F"),
                Summary = "Bất chấp bị hôn phu ruồng bỏ chỉ chưa đầy một tháng trước ngày trọng đại, Shasha vẫn mặc váy cô dâu chụp ảnh cưới."
            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Sóng gió hậu cung Thái Lan khi Hoàng quý phi trở lại",
                Status = Status.Active,
                Image = "https://i-ngoisao.vnecdn.net/2020/12/03/a1-9229-1606963626.jpg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("7C318165-4FD6-4FE2-A85C-069B08AE611F"),
                Summary = "Hoàng quý phi Sineenat trở lại hoàng cung Thái Lan sau gần một năm bị phế truất và bà cùng Hoàng hậu Suthida được cho là sẽ tiếp tục tranh giành địa vị cùng sự chú ý."
            });
            #endregion

            #region Category lam dep va cac bai viet
            modelBuilder.Entity<Categories>().HasData(
                    new Categories()
                    {
                        CategoryId = Guid.Parse("6D5883EF-15A1-4A3B-9420-4C49EA40FB14"),
                        CategoryName = "Làm đẹp",
                        DateCreated = DateTime.Now,
                        Status = Status.Active,
                    }
                    );


            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Ngừa mụn, sạch thâm sẹo từ nghệ và rau má: Điều bạn nên biết",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/10/20/ngua-mun-sach-tham-seo-tu-nghe-va-rau-madocx-1603159482179.jpeg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("6D5883EF-15A1-4A3B-9420-4C49EA40FB14"),
                Summary = "Nếu như Curcumin từ nghệ vàng hữu hiệu trong việc chăm sóc làn da, kháng viêm ngừa mụn; thì tinh chất rau má nổi bật với khả năng tăng cường chống lão hóa, giảm hình thành sẹo."

            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Aerial Yoga bộ môn của những cô nàng yêu cái đẹp",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/02/20/yoga-angel-dan-tri-final-1-docx-1582202127710.jpeg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("6D5883EF-15A1-4A3B-9420-4C49EA40FB14"),
                Summary = "Nếu bạn đang tìm kiếm một môn thể thao mang đầy tính nghệ thuật và thẩm mỹ, vậy thì chúc mừng, Aerial Yoga (Yoga bay) chính là sinh ra để dành cho bạn."
            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Điều gì cần quan tâm nhất khi đi làm răng?",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/09/29/2-dieu-gi-can-quan-tam-nhat-khi-di-lam-rang-2709-edit-02-docx-1601391275727.jpeg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("6D5883EF-15A1-4A3B-9420-4C49EA40FB14"),
                Summary = "Khi mua một món đồ có giá trị, người ta thường quan tâm đến cả chất lượng và vẻ đẹp của nó. Vậy khi có nhu cầu làm răng sứ hay Implant thì bạn quan tâm nhất điều gì?"
            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Bổ sung collagen mỗi ngày để có làn da khỏe đẹp",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/10/01/bo-sung-collagen-moi-ngay-de-co-lan-da-khoe-depdocx-1601531167675.png",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("6D5883EF-15A1-4A3B-9420-4C49EA40FB14"),
                Summary = "Lão hóa da là quy luật của tự nhiên, phụ nữ nên lựa chọn cho mình những sản phẩm làm đẹp phù hợp để ngăn ngừa quá trình này.Theo đó,lựa chọn các sản phẩm bổ sung collagen từ bên trong là giải pháp hiệu quả được nhiều phụ nữ tin dùng hiện nay.."
            });
            #endregion

            #region Category kinh doanh va cac bai viet
            modelBuilder.Entity<Categories>().HasData(
                    new Categories()
                    {
                        CategoryId = Guid.Parse("4A8AC1BD-586A-47B4-96EE-7CD12396E7DE"),
                        CategoryName = "Kinh doanh",
                        DateCreated = DateTime.Now,
                        Status = Status.Active,
                    }
                    );

            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Cổ phiếu Vietnam Airlines ra sao sau sai lầm của tiếp viên nhiễm Covid-19?",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/04/06/vietnam-airlines-1586188025160.jpg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("4A8AC1BD-586A-47B4-96EE-7CD12396E7DE"),
                Summary = "Cổ phiếu HVN của Vietnam Airlines giảm trong khi VN-Index vẫn duy trì đà tăng, tiền vào thị trường vẫn \"khỏe\"."

            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Những cách quản lý tài chính thông minh",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/12/03/dantri-post-event-confirmeddocx-1607008575349.jpeg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("4A8AC1BD-586A-47B4-96EE-7CD12396E7DE"),
                Summary = "Quản lý tài chính cá nhân là kỹ năng quan trọng trong xã hội hiện đại để sớm đạt tự do tài chính nhưng không phải ai cũng biết phải làm thế nào."
            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "USD suy giảm liên tục, dân giàu ôm đô la lo lắng",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/11/22/usd-suy-giam-lien-tuc-dan-giau-om-do-la-lo-lang-1-1606027686850.jpg",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("4A8AC1BD-586A-47B4-96EE-7CD12396E7DE"),
                Summary = "Thị trường tài chính thế giới diễn biến bất thường, đồng USD liên tục giảm trong những tuần gần đây. Nhiều dự báo khiến những người cầm đồng bạc xanh lo lắng khi mà kinh tế Việt Nam được cho là sẽ nằm top tăng trưởng cao nhất."
            });
            modelBuilder.Entity<Blogs>().HasData(new Blogs()
            {
                BlogId = Guid.NewGuid(),
                Title = "Tiền dư thừa ồ ạt đầu tư chứng khoán, cổ phiếu VCI của Bản Việt \"phá đỉnh\"",
                Status = Status.Active,
                Image = "https://icdn.dantri.com.vn/thumb_w/640/2020/12/02/screen-shot-20201202-at-123026-pm-1606890281582.png",
                DateCreated = DateTime.Now,
                CategoryId = Guid.Parse("4A8AC1BD-586A-47B4-96EE-7CD12396E7DE"),
                Summary = "Chỉ trong vòng 1 tháng qua, cổ phiếu VCI của Chứng khoán Bản Việt - công ty do bà Nguyễn Thanh Phượng sáng lập, tăng giá gần 19% và đã tăng tới 229% kể từ đáy."
            });
            #endregion

            // Tạo quyền admin
            var roleId = "8D04DCE2-969A-435D-BBA4-DF3F325983DC";
            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = roleId,
                Name = "admin",
                NormalizedName = "admin",
                
            });

            // Tạo tài khoản imic
            var userId = "69BD714F-9576-45BA-B5B7-F00649BE00DE";
            var hasher = new PasswordHasher<IdentityUser>();
            modelBuilder.Entity<IdentityUser>().HasData(new IdentityUser
            {
                Id = userId,
                UserName = "imic",
                NormalizedUserName = "imic",
                Email = "imic@gmail.com",
                NormalizedEmail = "imic@gmail.com",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "Abcd1234$"),
                SecurityStamp = string.Empty,
                PhoneNumber = "0971389875"
                
            });
            // gán quyền admin cho user imic
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = roleId,
                UserId = userId
            });
        }
    }
}
