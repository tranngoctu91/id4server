﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCore.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    CategoryName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Blogs",
                columns: table => new
                {
                    BlogId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    Title = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Image = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Summary = table.Column<string>(type: "nvarchar(2000)", maxLength: 2000, nullable: true),
                    Content = table.Column<string>(type: "ntext", nullable: true),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blogs", x => x.BlogId);
                    table.ForeignKey(
                        name: "FK_Blogs_Categories",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "CategoryId");
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "8D04DCE2-969A-435D-BBA4-DF3F325983DC", "e68a4f43-3f20-4e8c-bc19-45fe4f5e9a59", "admin", "admin" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "69BD714F-9576-45BA-B5B7-F00649BE00DE", 0, "a44ff277-aba2-45d3-80f8-24bcea958ea8", "imic@gmail.com", true, false, null, "imic@gmail.com", "imic", "AQAAAAEAACcQAAAAEDNluiIAeQKIScAMkuw28R1TXnW+gs73Ru8eUTkRvSG320f5MT8LDAufXhSBPSjA5A==", "0971389875", false, "", false, "imic" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "CategoryId", "CategoryName", "DateCreated", "Status" },
                values: new object[,]
                {
                    { new Guid("4a8ac1bd-586a-47b4-96ee-7cd12396e7de"), "Kinh doanh", new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3292), 1 },
                    { new Guid("6d5883ef-15a1-4a3b-9420-4c49ea40fb14"), "Làm đẹp", new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3180), 1 },
                    { new Guid("7c318165-4fd6-4fe2-a85c-069b08ae611f"), "Thời sự", new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3008), 1 }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "8D04DCE2-969A-435D-BBA4-DF3F325983DC", "69BD714F-9576-45BA-B5B7-F00649BE00DE" });

            migrationBuilder.InsertData(
                table: "Blogs",
                columns: new[] { "BlogId", "CategoryId", "Content", "DateCreated", "Image", "Status", "Summary", "Title" },
                values: new object[,]
                {
                    { new Guid("0c84c8ac-9206-4572-9ba5-301d7e390221"), new Guid("6d5883ef-15a1-4a3b-9420-4c49ea40fb14"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3197), "https://icdn.dantri.com.vn/thumb_w/640/2020/10/20/ngua-mun-sach-tham-seo-tu-nghe-va-rau-madocx-1603159482179.jpeg", 1, "Nếu như Curcumin từ nghệ vàng hữu hiệu trong việc chăm sóc làn da, kháng viêm ngừa mụn; thì tinh chất rau má nổi bật với khả năng tăng cường chống lão hóa, giảm hình thành sẹo.", "Ngừa mụn, sạch thâm sẹo từ nghệ và rau má: Điều bạn nên biết" },
                    { new Guid("1953c818-c1d3-4667-a7c7-b142506ed591"), new Guid("4a8ac1bd-586a-47b4-96ee-7cd12396e7de"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3388), "https://icdn.dantri.com.vn/thumb_w/640/2020/12/02/screen-shot-20201202-at-123026-pm-1606890281582.png", 1, "Chỉ trong vòng 1 tháng qua, cổ phiếu VCI của Chứng khoán Bản Việt - công ty do bà Nguyễn Thanh Phượng sáng lập, tăng giá gần 19% và đã tăng tới 229% kể từ đáy.", "Tiền dư thừa ồ ạt đầu tư chứng khoán, cổ phiếu VCI của Bản Việt \"phá đỉnh\"" },
                    { new Guid("2bcf90a5-7444-4a73-9bae-cd61a581b01b"), new Guid("7c318165-4fd6-4fe2-a85c-069b08ae611f"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3161), "https://i-ngoisao.vnecdn.net/2020/12/03/a1-9229-1606963626.jpg", 1, "Hoàng quý phi Sineenat trở lại hoàng cung Thái Lan sau gần một năm bị phế truất và bà cùng Hoàng hậu Suthida được cho là sẽ tiếp tục tranh giành địa vị cùng sự chú ý.", "Sóng gió hậu cung Thái Lan khi Hoàng quý phi trở lại" },
                    { new Guid("38e42ade-af0d-48d7-a8b0-18efba24159e"), new Guid("6d5883ef-15a1-4a3b-9420-4c49ea40fb14"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3237), "https://icdn.dantri.com.vn/thumb_w/640/2020/02/20/yoga-angel-dan-tri-final-1-docx-1582202127710.jpeg", 1, "Nếu bạn đang tìm kiếm một môn thể thao mang đầy tính nghệ thuật và thẩm mỹ, vậy thì chúc mừng, Aerial Yoga (Yoga bay) chính là sinh ra để dành cho bạn.", "Aerial Yoga bộ môn của những cô nàng yêu cái đẹp" },
                    { new Guid("3c7d5013-1ef2-411c-9863-f28b2cfe5e07"), new Guid("6d5883ef-15a1-4a3b-9420-4c49ea40fb14"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3274), "https://icdn.dantri.com.vn/thumb_w/640/2020/10/01/bo-sung-collagen-moi-ngay-de-co-lan-da-khoe-depdocx-1601531167675.png", 1, "Lão hóa da là quy luật của tự nhiên, phụ nữ nên lựa chọn cho mình những sản phẩm làm đẹp phù hợp để ngăn ngừa quá trình này.Theo đó,lựa chọn các sản phẩm bổ sung collagen từ bên trong là giải pháp hiệu quả được nhiều phụ nữ tin dùng hiện nay..", "Bổ sung collagen mỗi ngày để có làn da khỏe đẹp" },
                    { new Guid("6a83f432-aa18-4818-9437-e56c8edad9bc"), new Guid("6d5883ef-15a1-4a3b-9420-4c49ea40fb14"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3256), "https://icdn.dantri.com.vn/thumb_w/640/2020/09/29/2-dieu-gi-can-quan-tam-nhat-khi-di-lam-rang-2709-edit-02-docx-1601391275727.jpeg", 1, "Khi mua một món đồ có giá trị, người ta thường quan tâm đến cả chất lượng và vẻ đẹp của nó. Vậy khi có nhu cầu làm răng sứ hay Implant thì bạn quan tâm nhất điều gì?", "Điều gì cần quan tâm nhất khi đi làm răng?" },
                    { new Guid("9cc62d03-2a54-4128-971d-767e979139f8"), new Guid("4a8ac1bd-586a-47b4-96ee-7cd12396e7de"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3307), "https://icdn.dantri.com.vn/thumb_w/640/2020/04/06/vietnam-airlines-1586188025160.jpg", 1, "Cổ phiếu HVN của Vietnam Airlines giảm trong khi VN-Index vẫn duy trì đà tăng, tiền vào thị trường vẫn \"khỏe\".", "Cổ phiếu Vietnam Airlines ra sao sau sai lầm của tiếp viên nhiễm Covid-19?" },
                    { new Guid("9dc7eb07-5eba-4fe6-907b-80f3a4249c90"), new Guid("7c318165-4fd6-4fe2-a85c-069b08ae611f"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3096), "https://i1-ngoisao.vnecdn.net/2020/12/02/shutterstock1481861243-1606886-2241-9135-1606886554.jpg?w=300&h=180&q=100&dpr=1&fit=crop&s=T07hahBCERXkZ6HbbbaboA", 1, "Tôi chụp màn hình những tin nhắn gạ tình của chồng với 'sugar baby' đưa chồng xem nhưng anh vẫn chối.", "Chồng thả thính với 'sugar baby'" },
                    { new Guid("9eab9dff-40d0-484d-805b-1aca96d3b161"), new Guid("7c318165-4fd6-4fe2-a85c-069b08ae611f"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3143), "https://i-ngoisao.vnecdn.net/2020/12/03/mm-1-4343-1606986393.jpg", 1, "Bất chấp bị hôn phu ruồng bỏ chỉ chưa đầy một tháng trước ngày trọng đại, Shasha vẫn mặc váy cô dâu chụp ảnh cưới.", "Cô gái chụp ảnh cưới một mình sau 3 tuần bị hủy hôn" },
                    { new Guid("aefce992-e0ee-4713-a50c-a4a264308659"), new Guid("4a8ac1bd-586a-47b4-96ee-7cd12396e7de"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3369), "https://icdn.dantri.com.vn/thumb_w/640/2020/11/22/usd-suy-giam-lien-tuc-dan-giau-om-do-la-lo-lang-1-1606027686850.jpg", 1, "Thị trường tài chính thế giới diễn biến bất thường, đồng USD liên tục giảm trong những tuần gần đây. Nhiều dự báo khiến những người cầm đồng bạc xanh lo lắng khi mà kinh tế Việt Nam được cho là sẽ nằm top tăng trưởng cao nhất.", "USD suy giảm liên tục, dân giàu ôm đô la lo lắng" },
                    { new Guid("caa63b94-930a-48db-bd72-7391d881f970"), new Guid("4a8ac1bd-586a-47b4-96ee-7cd12396e7de"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3328), "https://icdn.dantri.com.vn/thumb_w/640/2020/12/03/dantri-post-event-confirmeddocx-1607008575349.jpeg", 1, "Quản lý tài chính cá nhân là kỹ năng quan trọng trong xã hội hiện đại để sớm đạt tự do tài chính nhưng không phải ai cũng biết phải làm thế nào.", "Những cách quản lý tài chính thông minh" },
                    { new Guid("f0d8b4d2-43af-4bab-9929-91e6d9af1c6e"), new Guid("7c318165-4fd6-4fe2-a85c-069b08ae611f"), null, new DateTime(2022, 8, 28, 14, 5, 23, 597, DateTimeKind.Local).AddTicks(3123), "https://i-ngoisao.vnecdn.net/2020/12/03/congchua1-7039-1606986354.jpg", 1, "Công chúa Aiko đón sinh nhật tuổi 19 hôm 1/12 khi đang tham gia các khóa học qua mạng tại trường đại học do ảnh hưởng của đại dịch Covid-19.", "Ảnh sinh nhật 19 tuổi của Công chúa Aiko'" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Blogs_CategoryId",
                table: "Blogs",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Blogs");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
