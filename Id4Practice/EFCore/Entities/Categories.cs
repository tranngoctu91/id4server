﻿using Domain.Shared.Enums;

using System;
using System.Collections.Generic;

namespace EFCore.Entities
{

    public  class Categories
    {
        public Categories()
        {
            Blogs = new List<Blogs>();
        }

        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Status Status { get; set; }
        public DateTime? DateCreated { get; set; }

        public ICollection<Blogs> Blogs { get; set; }
    }
}
