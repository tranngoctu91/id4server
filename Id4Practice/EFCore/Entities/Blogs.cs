﻿
using Domain.Shared.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCore.Entities
{

    public  class Blogs
    {
        public Guid BlogId { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public Guid? CategoryId { get; set; }
        public Status Status { get; set; }
        public DateTime? DateCreated { get; set; }

        public Categories Category { get; set; }
    }
}
