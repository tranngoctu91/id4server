﻿
using EFCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Configurations
{
    public class BlogConfiguration : IEntityTypeConfiguration<Blogs>
    {
        public void Configure(EntityTypeBuilder<Blogs> entity)
        {
            entity.HasKey(e => e.BlogId);

            entity.Property(e => e.BlogId).HasDefaultValueSql("(newid())");

            entity.Property(e => e.Content).HasColumnType("ntext");

            entity.Property(e => e.DateCreated).HasColumnType("datetime");

            entity.Property(e => e.Image).HasMaxLength(500);

            entity.Property(e => e.Summary).HasMaxLength(2000);

            entity.Property(e => e.Title).HasMaxLength(500);

            entity.HasOne(d => d.Category)
                .WithMany(p => p.Blogs)
                .HasForeignKey(d => d.CategoryId)
                .HasConstraintName("FK_Blogs_Categories");
        }
    }
}
