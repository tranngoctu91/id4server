﻿
using EFCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Configurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Categories>
    {
        public void Configure(EntityTypeBuilder<Categories> entity)
        {
            entity.HasKey(e => e.CategoryId);

            entity.Property(e => e.CategoryId).HasDefaultValueSql("(newid())");

            entity.Property(e => e.CategoryName).HasMaxLength(500);

            entity.Property(e => e.DateCreated).HasColumnType("datetime");
        }
    }
}
