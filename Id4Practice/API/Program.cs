﻿using Domain.Shared.Demos;

using EFCore.EF;
using FluentValidation.AspNetCore;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddControllers();
// dòng này để register fluentvalidation
builder.Services.AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<DemoVm>());

// dòng này để kích hoạt validation atribute
builder.Services.Configure<ApiBehaviorOptions>(o =>

    {
        o.SuppressModelStateInvalidFilter = true;
    });

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



//config identityserver authentication
builder.Services.AddAuthentication(
                IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication("Bearer", options =>
               {
                   options.Authority = "https://localhost:7263/";
                   options.ApiName = "practiceApi";
               });



//config entityframework sql server
builder.Services.AddEntityFrameworkSqlServer();
builder.Services.AddDbContext<BlogDbContext>(option => option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<BlogDbContext>();



builder.Services.AddTransient<UserManager<IdentityUser>, UserManager<IdentityUser>>();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();
