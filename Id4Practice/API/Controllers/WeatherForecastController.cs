using API.Helpers;
using Domain.Shared.Demo1s;
using Domain.Shared.Demos;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        [Route("demo")]
        public IActionResult Demo()
        {
            return Ok("get th�nh c�ng");
        }

        [HttpPost]
        [Route("add-demo")]
        public IActionResult AddDemo( DemoVm model)
        {
            return Ok("add th�nh c�ng");
        }
        [HttpPost]
        [Route("add-demo1")]
        [ApiValidationFilterAttribute]
        public IActionResult AddDemo1(Demo1Vm model)
        {
            return Ok("add th�nh c�ng");
        }
    }
}