﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.Demo1s
{
    public class Demo1VmValidator : AbstractValidator<Demo1Vm>
    {
        public Demo1VmValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Age).GreaterThan(10);
        }
    }
}
