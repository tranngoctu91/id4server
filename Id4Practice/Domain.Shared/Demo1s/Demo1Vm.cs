﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.Demo1s
{
    public class Demo1Vm
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
