﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.Demos
{
    public class DemoVm
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
