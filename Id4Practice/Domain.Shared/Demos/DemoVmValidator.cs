﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.Demos
{
    public class DemoVmValidator : AbstractValidator<DemoVm>
    {
        public DemoVmValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Age).GreaterThan(5);
        }
    }
}
