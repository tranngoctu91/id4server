﻿using IdentityServer4.Models;

namespace IdS4Server
{
    public class Config
    {
        // tra ve danh sach các identity resource
        public static IEnumerable<IdentityResource> IdentityResources =>
                new List<IdentityResource>()
                {
                    new IdentityResources.OpenId(),
                    new IdentityResources.Profile(),
                    new IdentityResources.Email(),
                    new IdentityResources.Phone(),
                };

        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>()
            {
                new ApiScope("practiceApi.read")
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new List<ApiResource>()
            {
                new ApiResource("practiceApi")
                {
                    Scopes = new List<string>{"practiceApi.read"},
                    ApiSecrets = new List<Secret> {new Secret("superscret".Sha256())}
                }
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>()
            {
                new Client
                {
                    ClientId = "cwm.client",
                    ClientName = "Client Credentials Client",
                    //dòng này để có thể authen bằng username và password
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    ClientSecrets = {new Secret ("secret".Sha256())},
                    AllowedScopes = { "practiceApi.read" }
                }
            };
    }
}
